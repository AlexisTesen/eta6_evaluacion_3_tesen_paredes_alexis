package com.cibertec.action;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cibertec.entidad.Client;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("servicios")
public class ClientAction extends ActionSupport {

	private String URL_CLIENTE = "http://localhost:8080/CL3_Server_Tesen/cl3Rest/client/";

	private Client client;
	private Client[] clientList;
	private int ageOne;
	private int ageTwo;

	private HttpClient httpClient;

	public ClientAction() {
		httpClient = HttpClientBuilder.create().build();
	}

	@Action(value = "/save", results = { @Result(name = "ok", type = "redirect", location = "/cliente.jsp") })
	public String save() throws ClientProtocolException, IOException {

		Gson gson = new Gson();
		String dataJSON;
		dataJSON = gson.toJson(client);
		HttpPost httpPost = new HttpPost(URL_CLIENTE + "save");
		StringEntity string = new StringEntity(dataJSON.toString(), "UTF-8");
		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
		httpPost.setEntity(string);
		HttpResponse httpResponse = httpClient.execute(httpPost);
		return "ok";

	}

	@Action(value = "/listAllClientes", results = { @Result(name = "ok", type = "json") })
	public String listAllClientes() throws ClientProtocolException, IOException {

		HttpGet httpGet = new HttpGet(URL_CLIENTE + "list/" + ageOne + "/" + ageTwo);
		HttpResponse httpResponse = httpClient.execute(httpGet);
		String json = EntityUtils.toString(httpResponse.getEntity());
		Gson gson = new Gson();
		clientList = gson.fromJson(json, Client[].class);
		return "ok";
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client[] getClientList() {
		return clientList;
	}

	public void setClientList(Client[] clientList) {
		this.clientList = clientList;
	}

	public int getAgeOne() {
		return ageOne;
	}

	public void setAgeOne(int ageOne) {
		this.ageOne = ageOne;
	}

	public int getAgeTwo() {
		return ageTwo;
	}

	public void setAgeTwo(int ageTwo) {
		this.ageTwo = ageTwo;
	}

}
