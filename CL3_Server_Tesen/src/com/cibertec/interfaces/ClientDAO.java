package com.cibertec.interfaces;

import java.util.List;

import com.cibertec.entidad.Client;

public interface ClientDAO {
	
	public int clientSave(Client bean);
	public List<Client> clientListForAge(int ageOne,int ageTwo );

}
