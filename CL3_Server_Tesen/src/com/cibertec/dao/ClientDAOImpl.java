package com.cibertec.dao;

import java.util.List;

import com.cibertec.entidad.Client;
import com.cibertec.interfaces.ClientDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import utils.MysqlDBConexion;

public class ClientDAOImpl implements ClientDAO {

	@Override
	public int clientSave(Client bean) {
		int state=-1;
		Connection cn=null;
		PreparedStatement cstm=null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="insert into tb_client (name,lastname,dni,age) values(?,?,?,?)";
			cstm=cn.prepareStatement(sql);
			cstm.setString(1,bean.getName());
			cstm.setString(2, bean.getLastname());
			cstm.setString(3,bean.getDni());
			cstm.setInt(4, bean.getAge());
			state=cstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(cstm!=null) cstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return state;
	}

	@Override
	public List<Client> clientListForAge(int ageOne, int ageTwo) {
		List<Client> list=new ArrayList<Client>();
		Client bean=null;
		Connection cn=null;
		PreparedStatement cstm=null;
		ResultSet rs=null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="select * from tb_client where age>=? and age<=?";
			cstm=cn.prepareStatement(sql);
			cstm.setInt(1,ageOne);
			cstm.setInt(2,ageTwo);
			rs=cstm.executeQuery();
			while(rs.next()) {
				bean=new Client();
				bean.setIdClient(rs.getInt(1));
				bean.setName(rs.getString(2));
				bean.setLastname(rs.getString(3));
				bean.setDni(rs.getString(4));
				bean.setAge(rs.getInt(5));
				list.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(rs!=null) rs.close();
				if(cstm!=null) cstm.close();
				if(cn!=null) cn.close();
			} catch (Exception ex2) {
				ex2.printStackTrace();
			}
		}
		return list;
	}

}
