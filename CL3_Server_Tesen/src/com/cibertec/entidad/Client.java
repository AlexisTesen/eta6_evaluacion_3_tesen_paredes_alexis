package com.cibertec.entidad;

import java.io.Serializable;

public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int idClient;
	private String name;
	private String lastname;
	private String dni;
	private int age;

	public Client(int idClient, String name, String lastname, String dni, int age) {
		super();
		this.idClient = idClient;
		this.name = name;
		this.lastname = lastname;
		this.dni = dni;
		this.age = age;
	}

	public Client() {

	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
