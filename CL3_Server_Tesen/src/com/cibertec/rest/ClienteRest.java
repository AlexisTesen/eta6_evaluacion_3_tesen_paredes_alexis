package com.cibertec.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cibertec.dao.ClientDAOImpl;
import com.cibertec.entidad.Client;

@Path("/client")
public class ClienteRest {
	private ClientDAOImpl dao = new ClientDAOImpl();

	@GET
	@Path("/list/{ageOne}/{ageTwo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listaCliente(@PathParam("ageOne") int ageOne, @PathParam("ageTwo") int ageTwo) {
		return Response.ok(dao.clientListForAge(ageOne, ageTwo)).build();
	}

	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public int registrarCliente(Client bean) {
		return dao.clientSave(bean);
	}

}
